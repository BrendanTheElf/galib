newf = open("results.table", "w+")

open("results.txt", "r") do f
    for line in eachline(f)
        contains(line, "Job") ? continue : nothing
        nnewl = replace(line, " ", "")
        newl = replace(nnewl, "--", ",")
        nnnewl = replace(newl, r"[ζ0tλnμ]=|α_", "")
        write(newf, replace(nnnewl, ",", "\t"))
        write(newf, "\n")
    end
end
close(newf)

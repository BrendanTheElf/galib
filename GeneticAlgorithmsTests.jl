using Base.Test
include("GeneticAlgorithms.jl")
using GeneticAlgorithms


@testset "BitFlip" begin
    ind = Individual(zeros(Int8, 10))
    bitflip!(ind.gene, 1)
    @test ind.gene == ones(Int8, 10)
    srand(1234)
    @test bitflip!(ind.gene, .5) == bitflip!(ind.gene, .5)
end


@testset "Reset" begin
    ga = runga()
    @assert ga.numevals > 0
    old =  ga.population[1]
    reset!(ga)
    @test ga.numevals == 0
    @test !isempty(ga.population)
    @test isa(ga.population[1].fitness, Number)
    @test ga.population[1] != old
end


@testset "Evaluate" begin
    const n = 10
    ga = GAModel(n)
    ga.population = [newindividual(ga)]
    ga.population[1].gene = zeros(Int8, n)
    @test ga.population[1].fitness == -1
    @test evaluate!(ga) == nothing && !ga.success && ga.population[1].fitness != -1
    ga.population[1].gene = ones(Int8, n)
    @test evaluate!(ga) != nothing && ga.success
end


@testset "Substring" begin
    const k = 3
    ind0 = Individual(repeat([0, 1], outer=5))
    ind1 = Individual(repeat([1, 0], outer=5))
    ind2 = Individual([ones(4); repeat([0, 1], outer=3)])
    ind3 = Individual([zeros(5); [1, 1, 1, 1, 0]])
    ind4 = Individual([[0, 1, 0]; ones(7)])
    ind5 = Individual([ones(9); [0]])
    map(ind -> ind.fitness = substring(ind, k), [ind0, ind1, ind2, ind3, ind4, ind5])
    @test ind0.fitness == 0
    @test ind1.fitness == 1
    @test ind2.fitness == 4
    @test ind3.fitness == 9
    @test ind4.fitness == 10
    @test ind5.fitness == 9
end


@testset "Leading Ones" begin
    ind1 = Individual([ones(Int8, 5); zeros(Int8, 5)])
    ind2 = Individual([[1, 1, 1, 1, 0]; ones(Int8, 5)])
    ind3 = Individual(repeat([1, 0], outer=5))
    @test leadingones(ind1) > leadingones(ind2) > leadingones(ind3)
    ind2.gene[5] = 1
    @test leadingones(ind1) < leadingones(ind2)
end


@testset "One Max" begin
    ind1 = Individual(zeros(Int8, 10))
    ind2 = Individual(repeat([1, 0], outer=5))
    ind3 = Individual([[0, 1, 1, 1, 0]; ones(Int8, 5)])
    ind4 = Individual(ones(Int8, 10))
    map(ind -> ind.fitness = onemax(ind), [ind1, ind2, ind3, ind4])
    @test ind1.fitness == 0
    @test ind2.fitness == 5
    @test ind3.fitness == 8
    @test ind4.fitness == 10
end


@testset "Peak" begin
    const k = 4
    ind1 = Individual(zeros(Int8, 10))
    ind2 = Individual(repeat([0, 1], outer=5))
    ind3 = Individual([ones(4); repeat([0, 1], outer=3)])
    ind4 = Individual(ones(Int8, 10))
    map(ind -> ind.fitness = peak(ind, k), [ind1, ind2, ind3, ind4])
    @test ind1.fitness == 4
    @test ind2.fitness == 0
    @test ind3.fitness == 4
    @test ind4.fitness == 10
end


@testset "Royal Road" begin
    const k = 3
    const n = 9
    const block = ones(Int8, k)
    ind1 = Individual(zeros(Int8, n))
    ind2 = Individual(repeat([0, 1, 0], outer=3))
    ind3 = Individual([[0, 1, 1]; repeat([1, 1, 0], outer=2)])
    ind4 = Individual([block; [1, 0, 1]; block])
    ind5 = Individual([block; block; block])
    map(ind -> ind.fitness = royalroad(ind, k), [ind1, ind2, ind3, ind4, ind5])
    @test ind1.fitness == 0
    @test ind2.fitness == 0
    @test ind3.fitness == 0
    @test ind4.fitness == 2
    @test ind5.fitness == n
end


@testset "Jump" begin
    const k = 4
    const n = 10
    ind1 = Individual(zeros(n))
    ind2 = Individual(repeat([1, 0], outer=(n ÷ 2)))
    ind3 = Individual([zeros(k); ones(n-k)])
    ind4 = Individual([[1, 0, 0, 0]; ones(n-k)])
    ind5 = Individual(ones(n))
    map(ind -> ind.fitness = jump(ind, k), [ind1, ind2, ind3, ind4, ind5])
    @test ind1.fitness == k
    @test ind2.fitness == n ÷ 2 + k
    @test ind3.fitness == n
    @test ind4.fitness == k - 1
    @test ind5.fitness == n + 1
end

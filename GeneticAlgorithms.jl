module GeneticAlgorithms

type Individual
    gene::Array{Int8}
    mprob::Number
    fitness::Number

    Individual(gene) = new(gene, 1, -1)
    Individual(gene, mprob) = new(gene, mprob, -1)
end
export Individual


Base.isless(lhs::Individual, rhs::Individual) = lhs.fitness < rhs.fitness

mutbiasisless(lhs::Individual, rhs::Individual) = (lhs.fitness == rhs.fitness ? lhs.mprob < rhs.mprob : lhs.fitness < rhs.fitness)


type GAModel
    popsize::Int
    parsize::Int
    numevals::Int
    n::Int
    population::Array{Individual}
    mprob::Number
    eval::Function
    select::Function
    mutate::Function
    maxevals::Int
    max::Number
    start # TODO: make this nullable or something
    success::Bool
    display::Bool

    GAModel(n) = new(1, 1, 0, n, [], 1/n, leadingones, mulambda, standardmut!, n^3, n, nothing, false, false)
    GAModel(popsize, parsize, n, mprob, eval, select, mutate, maxevals, max, start, display) =
        new(popsize, parsize, 0, n, [], mprob, eval, select, mutate, maxevals, max, start, false, display)
end
export GAModel

## TODO: support regex-matched command line arguments; plot mechanism; fix tournament selection


function runga(; popsize=1, parsize=1, n=100, mprob=1/n, eval=leadingones,
               select=mulambda, mutate=standardmut!, elitist=true, maxevals=n^3, max=n, start=nothing, display=false)
    
    model = GAModel(popsize, parsize, n, mprob, eval, select, mutate, maxevals, max, start, display)
    if elitist
        return elitistga(model)
    end
    return nonelitistga(model)
end
export runga


function elitistga(model::GAModel)
    reset!(model)
    while model.numevals < model.maxevals
        model.numevals += 1
        parents = deepcopy(model.population)
        for i in 1:model.popsize
            child = model.mutate(deepcopy(parents[model.select(model)]))
            push!(model.population, child)
        end
        if evaluate!(model) != nothing
            sort!(model.population; rev=true)
            model.numevals *= model.popsize
            return model
        end
        sort!(model.population; rev=true)
        resize!(model.population, model.popsize)
    end
    model.numevals *= model.popsize
    model
end


function nonelitistga(model::GAModel)
    reset!(model)
    while model.numevals < model.maxevals
        model.numevals += 1
        children = []
        for i in 1:model.popsize
            child = model.mutate(deepcopy(model.population[model.select(model)]))
            push!(children, child)
        end
        model.population = children
        if evaluate!(model) != nothing
            sort!(model.population; rev=true)
            model.numevals *= model.popsize
            return model
        end
        sort!(model.population; rev=true)
    end
    model.numevals *= model.popsize
    model
end


function reset!(model::GAModel)
    model.numevals = 0
    empty!(model.population)
    for i = 1:model.popsize
        push!(model.population, newindividual(model))
    end
    evaluate!(model)
    sort!(model.population; rev=true)
end
export reset!


function evaluate!(model::GAModel)
    for ind in model.population
        ind.fitness = model.eval(ind)
        if ind.fitness == model.max
            model.success = true
            return ind
        end
    end
end
export evaluate!


function newindividual(model::GAModel)
    if model.start == nothing
        return Individual(rand(0:1, model.n), model.mprob)
    end
    Individual(model.start, model.mprob)
end
export newindividual 


function bitflip!(gene::Array{Int8, 1}, p::Number)
    @assert p > 0 && p <= 1
    for i in 1:endof(gene)
        r = rand()
        r <= p ? gene[i] = (gene[i] + 1) % 2 : continue
    end
end
export bitflip!


standardmut!(ind::Individual) = (bitflip!(ind.gene, ind.mprob); ind)
export standardmut!


function trialsamut!(ind::Individual, eval::Function=leadingones, A::Number=1.3, b::Number=.85)
    y = standardmut!(deepcopy(ind))
    eval(y) < ind.fitness ? ind.mprob *= b : ind.mprob *= A
    ind.mprob = min(1, max(0, ind.mprob))
    standardmut!(ind)
end
export trialsamut!


function simplesamut!(ind::Individual, A::Number=1.3, b::Number=.85)
    rand() <= 0.75 ? ind.mprob *= b : ind.mprob *= A
    ind.mprob = min(1, max(0, ind.mprob))
    standardmut!(ind)
end
export simplesamut!


function leadingones(ind::Individual)
    score = 0
    for x in ind.gene
        if x == 0
            return score
        end
        score = score + 1
    end
    score
end
export leadingones


function substring(ind::Individual, k::Int64=(endof(ind.gene) ÷ 10))
    score = 0
    for i in 1:endof(ind.gene)
        start = max(i - k, 1)
        if prod(ind.gene[start:i]) == 1
            score = i
        end
    end
    score
end
export substring


function onemax(ind::Individual)
    sum(ind.gene)
end
export onemax


function peak(ind::Individual, k::Int64=(endof(ind.gene) ÷ 20))
    if ind.gene == zeros(Int8, endof(ind.gene))
        return k
    end
    leadingones(ind)
end
export peak


function royalroad(ind::Individual, k::Int64=5)
    n = endof(ind.gene)
    @assert n % k == 0
    score = 0
    for i in 1:(n ÷ k)
        score += prod(ind.gene[k * (i - 1) + 1: k * i])
    end
    if score == n ÷ k
        return n
    end
    score
end
export royalroad


function jump(ind::Individual, k::Int64=3)
    om = onemax(ind)
    n = endof(ind.gene)
    score = 0
    if om == n
        score = n + 1
    elseif om <= n - k
        score = k + om
    else
        score = n - om
    end
    score
end
export jump
    

function mulambda(model::GAModel)
    @assert model.parsize <= model.popsize
    rand(1:model.parsize)
end
export mulambda


function mutmulambda(model::GAModel)
    sort!(model.population, lt=mutbiasisless, rev=true)
    mulambda(model)
end
export mutmulambda
    

function tournament(model::GAModel, k=2)
    cand = []
    for i in 1:k
        push!(cand, model.population[rand(1:model.popsize)])
    end
    sort!(cand, rev=true)
    cur = pop!(cand)
    while isless(cur, cand[1])
        cur = pop!(cand)
    end
    cand[rand(1:length(cand))]
end


function printprogress(model::GAModel)
    println("t: $(model.numevals)")
    for ind in model.population
        println(join(ind.gene, " "), " --- ", ind.fitness)
    end
    sleep(.5)
end


function printresults(model::GAModel, x...)
    print("n=$(model.n), λ=$(model.popsize), μ=$(model.parsize), t=$(model.numevals), ")
    model.success ? print("success") : print("failure")
    length(x) > 0 ? println(" -- ", join(x, ", ")) : println()
end
export printresults


function resultstable(data...)
    for d in data
        print(replace(string(d), r"(.+\.)|!", ""), "\t")
    end
    println()
end
export resultstable


# function fitxmut(fits, muts)
#     names(plt)
#     if !isdefined(:PyPlot)
#         # eval(Expr(:using,:PyPlot))
#         print("Please include 'using PyPlot' in your script")
#     end
#     #plt[:cla]() <- does not work
#     plt[:ylim](0,1)
#     d = 1:10
#     plot(d, d.^2)
#     plot(fits, muts)
#     title("Population fitness vs. mutation probability")
# end

end # END MODULE

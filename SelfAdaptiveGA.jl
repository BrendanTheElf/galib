module SelfAdaptiveGA

importall GeneticAlgorithms

type SelfAdaptiveIndividual <: Individual
    gene::Array{Int}
    mut_rate
    fitness

    SelfAdaptiveIndividual(gene, mut_rate) = new(gene, mut_rate, nothing)
end

create_individual(size) = SelfAdaptiveIndividual(rand(0:1, size), rand() * (size/2-.2) - .2)

function mutate(ind::Individual)
    if rand() <= 0.1
        ind.mut_rate = rand() * (length(ind.gene)/2-.2) -.2
    end
    for i in 1:length(ind.gene)
        rand() <= (ind.mut_rate/length(ind.gene)) ? ind.gene[i] = (ind.gene[i] + 1) % 2 : continue
    end
end


end
